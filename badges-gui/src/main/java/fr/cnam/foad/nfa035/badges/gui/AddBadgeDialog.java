package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.jdesktop.swingx.JXDatePicker;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Date;

public class AddBadgeDialog extends JDialog implements PropertyChangeListener  {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField codeSerie;
    private JFileChooser fileChooser;

    private JXDatePicker dateDebut;
    private JXDatePicker dateFin;
    private DigitalBadge badge;
    private DirectAccessBadgeWalletDAO dao;
    private BadgeWalletGUI caller;

    public AddBadgeDialog() {
        fileChooser.setFileFilter(new FileNameExtensionFilter(" *.jpg,*.png,*.jpeg,*.gif", "jpg","png","jpeg","gif"));
        fileChooser.addPropertyChangeListener(this);
        codeSerie.addPropertyChangeListener(this);
        dateDebut.addPropertyChangeListener(this);
        dateFin.addPropertyChangeListener(this);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    public void setDao(DirectAccessBadgeWalletDAO dao) {
        this.dao = dao;
    }
    public void setCaller(BadgeWalletGUI caller) {
        this.caller = caller;
    }

    /**
     * execute les actions devant suivre la selection du bouton OK
     */
    private void onOK() {
        // add le badge saisi à bla liste à l'aide du DAO.
        try {
            dao.addBadge(badge);
            caller.setAddedBadge(badge);
        } catch (IOException exception)
        { exception.printStackTrace();}


        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        AddBadgeDialog dialog = new AddBadgeDialog();
        dialog.pack();
        dialog.setLocationRelativeTo(null);
        dialog.setIconImage(dialog.getIcon().getImage());
        dialog.setVisible(true);
        System.exit(0);
    }
    /**
     * Valide si les champs sélectionnées suffise à la constitution d'un badge
     * @return
     */
    private boolean validateForm(){
        File image = fileChooser.getSelectedFile();
        String codeSerieStr = codeSerie.getText();
        Date fin = dateFin.getDate();
        Date debut = dateDebut.getDate();
        if (codeSerieStr != null && codeSerieStr.trim().length() > 4 && image != null && image.exists()
                && ((fin != null && debut == null)||(fin != null && fin.after(debut)))){
            this.badge = new DigitalBadge(codeSerieStr,debut,fin,null,image);
            return true;
        }
        else{
            return false;
        }
    }
    /**
     * {@inheritDoc}
     * @param evt
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (validateForm()){
            this.buttonOK.setEnabled(true);
        }
        else{
            this.buttonOK.setEnabled(false);
        }
    }
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/logo.png"));
    }

}
