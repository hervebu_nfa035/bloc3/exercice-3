package fr.cnam.foad.nfa035.badges.gui.renderer;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

/**
 * Gestion du rendu (formattage du contenu et couleurs (Background, Foreground)
 */
public class BadgeSizeCellRenderer extends DefaultTableCellRenderer {

    Color originBackground;
    Color originForeground;

    /**
     * Constructeur : Initialisation des couleurs (Background,Foreground) à partir des couleurs
     * de la cellule avant l'opération de rendu (Renderer).
     */
    public BadgeSizeCellRenderer() {
        super();
        this.originBackground = this.getBackground();
        this.originForeground = this.getForeground();
    }

    /**
     * Gestion des couleurs d'une cellule du tableau (Foreground (caracteres) , Background (fonds) en fonction de
     * la taille et de la selection (isSelected) :
     * Background color --> Orange si (Taille > 10000 et si isSelected = false)
     * Foreground color --> Red si (Taille > 10000 et si isSelected = true)
     * @param table le composant JTable
     * @param value L'objet
     * @param isSelected Si sélectionné
     * @param hasFocus Si focus
     * @param row indice de ligne
     * @param column indice de colonne
     * @return Component
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus,    row, column);

        Long size = (Long) value;
        setText(humanReadableByteCountBin(size));
        if (!isSelected) {
            setForeground(this.originBackground);
            if (size > 10000) {
                setBackground(Color.ORANGE);
            } else {
                setBackground(this.originBackground);
            }
        }
        else{
            if (size > 10000) {
                setForeground(Color.RED);
            } else {
                setForeground(this.originBackground);
            }
        }

        return comp;
    }

    /**
     * formatter l'affichage d'une variable de type long (entier)
     * @param bytes la taille formattée à l'échelle
     * @return String
     */
    private String humanReadableByteCountBin(long bytes) {
        long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        if (absB < 1024) {
            return bytes + " o";
        }
        long value = absB;
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");
        for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
            value >>= 10;
            ci.next();
        }
        value *= Long.signum(bytes);
        return String.format("%.1f %co", value / 1024.0, ci.current());
    }
}